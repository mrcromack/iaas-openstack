heat_template_version: wallaby
description:  Lua no OpenResty com MySQL

parameters:
  app_image:
    type: string
    description: imagem utilizada pelo openresty
    default: debian-11-amd64
  app_key:
    type: string 
    description: chave ssd app_server 
    default: mit
  database_image:
    type: string
    description: imagem utilizada pelo openresty
    default: debian-11-mysql-amd64
  database_key:
    type: string 
    description: chave ssh database_server
    default: mit
  flavor:
    type: string
    description: Maquina New
    default: mit.micro
  network:
    type: string
    description: rede APP
    default: apps
  subnet:
    type: string 
    description: rede
    default: apps-subnet    
  floating_network:
    type: string 
    default: private

resources:
      
  database_password:
    type: OS::Heat::RandomString
  
  database_root_password:
    type: OS::Heat::RandomString

  lua_app_security_group:
    type: OS::Neutron::SecurityGroup
    properties:
      name: "lua_app_security_group"
      description: Permite ICMP, SSH, HTTP & HTTPS
      rules:
      - { direction: ingress, protocol: icmp }
      - { direction: ingress, protocol: tcp, port_range_min: 22, port_range_max: 22 }
      - { direction: ingress, protocol: tcp, port_range_min: 80, port_range_max: 80 }
      - { direction: ingress, protocol: tcp, port_range_min: 443, port_range_max: 443 }

  lua_database_security_group:
    type: OS::Neutron::SecurityGroup
    properties:
      name: "lua_database_security_group"
      description: Permite ICMP, SSH, Mysql
      rules:
      - { direction: ingress, protocol: icmp }
      - { direction: ingress, protocol: tcp, port_range_min: 22, port_range_max: 22 }
      - { direction: ingress, protocol: tcp, port_range_min: 3306, port_range_max: 3306 }

  lua_network:
    type: OS::Neutron::Net
    properties:
      name: { get_param: network }
      value_specs:
        mtu: 1442

  lua_subnet:
    type: OS::Neutron::Subnet
    properties:
      name: { get_param: subnet }
      network_id: { get_resource: lua_network }
      cidr: "192.168.20.0/24"
      dns_nameservers: [ "8.8.8.8", "8.8.4.4"]
      ip_version: 4

  lua_router:
    type: OS::Neutron::Router
    properties:
      name:  'lua-router'
      external_gateway_info: { network: public }

  lua_router_subnet_interface:
    type: OS::Neutron::RouterInterface
    properties:
      router_id: { get_resource: lua_router }
      subnet: { get_resource: lua_subnet }

  lua_port:
    type: OS::Neutron::Port
    properties:
      network: { get_resource: lua_network }
      security_groups: [ { get_resource: lua_app_security_group } ]
      fixed_ips:
      - subnet_id: { get_resource: lua_subnet }

  lua_floating:
    type: OS::Neutron::FloatingIP
    properties:
      floating_network: { get_param: floating_network }
      port_id: { get_resource: lua_port }

  database_server:
    type: OS::Nova::Server
    depends_on: [lua_router, lua_subnet, lua_floating]  
    properties:
      security_groups: [{ get_resource: lua_database_security_group }]
      flavor: {get_param: flavor }
      image: {get_param: database_image }
      key_name: {get_param: database_key }
      networks:
        - port: {get_resource: lua_port}
      user_data:
        str_replace:
          template: |
            #!/bin/bash
            sed -i s/127.0.0.1/0.0.0.0/ /etc/mysql/mariadb.conf.d/50-server.cnf
            cat <<EOF | mysql
            CREATE DATABASE $db_name;
            CREATE USER $db_user@'%' IDENTIFIED BY '$db_password';
            GRANT ALL PRIVILEGES ON *.* TO $db_user@'%';
            EOF
            systemctl restart mysql
          params:
            $db_password: {get_attr: [database_password, value]}
            $db_root_password: {get_attr: [database_root_password, value]}
            $db_user: debian
            $db_name: debian_db
  app_server:
    type: OS::Nova::Server
    depends_on: [ lua_router, database_server ]
    properties:
      security_groups: [ { get_resource: lua_database_security_group } ]
        #security_groups:
        #- get_resource: lua_app_security_group
      flavor: { get_param: flavor }
      image: { get_param: app_image }
      key_name: { get_param: app_key }
      networks: [ {port: { get_resource: lua_port}}]
      user_data:
        str_replace:
          template: |
            #!/bin/bash
            apt-get update
            apt-get -y install --no-install-recommends wget gnupg ca-certificates
            wget -O - https://openresty.org/package/pubkey.gpg | sudo apt-key add -
            echo "deb http://openresty.org/package/debian buster openresty" > /etc/apt/sources.list.d/openresty.list
            apt-get update
            apt-get -y install openresty luarocks lua-inspect lua-sql-mysql gcc libssl-dev liblua5.1-dev
            luarocks install lapis
            wget https://github.com/hector-vido/lua-ms/archive/refs/heads/master.zip -O /tmp/master.zip
            unzip /tmp/master.zip -d /tmp/
            mkdir -p /opt/app/
            mv /tmp/lua-ms-master/* /opt/app/
            sed 's/8080/80/' /opt/app/docker/nginx.conf > /etc/openresty/nginx.conf
            cat > /opt/app/config.lua <<EOF
            -- config.lua
            local config = require('lapis.config')
            config('development', {
              secret = 'UxNGNjv5qjbmhpbRjTnVNGknUEzBQnee',
              mysql = {
                host = '$db_ip',
                port = '3306',
                user = '$db_user',
                password = '$db_password',
                database = '$db_name'
              }
            })
            EOF
            systemctl restart openresty
          params:
            $db_ip: {get_attr: [database_server, first_address]}
            $db_user: debian
            $db_name: debian_db
outputs:
  mysql_root_password:
    value:
      str_replace:
        template: PASSWORD
        params:
          PASSWORD: {get_attr: [database_root_password, value]}
    description: "Database root password"

  mysql_password:
    value:
      str_replace:
        template: PASSWORD
        params:
          PASSWORD: {get_attr: [database_password, value]}
    description: "Database user password"

  server_IP:
    value:
      str_replace:
        template: IP
        params:
          IP: { get_attr: [lua_floating, floating_ip_address] }
    description: "IP"

