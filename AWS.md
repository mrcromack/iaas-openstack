# AWS

Para o ambiente do curso precisaremos de uma máquina (EC2), uma chave de acesso (key pair) e um security group completamente aberto.

## Key Pair

Para criar uma chave de acesso SSH clique no ícone "Services" no painel da AWS, selecione **EC2** e na página que aparecer clique em **Key Pairs**.

- Crie um keypair chamado `openstack` e faça download da chave.

> Guarde esta chave pois será utilizada para se logar na máquina.

## Security Group

Para evitar problemas de conexão e facilitar o entendimento, utilizaremos um security group completamente aberto, para isso, volte a página anterior e clique em **Security Groups**.

- Crie um security group chamado `openstack`;
- Na descrição escreva apenas `Openstack`;
- Em **Inbound rules** adicione uma regra de tipo `All Trafic`, com o source `Anywhere-IPv4`;
- Em **Inbound rules** adicione uma segunda regra de tipo `All Trafic`, com o source `Anywhere-IPv6`;

## Máquina

Para criar a máquina volte a página anterior e clique no botão laranja **Launch Istance**.

Para o ambiente - infelizmente - utilizaremos apenas uma máquina `t2.large` com 8GB de RAM e um disco de 100GB:
- No campo nome, especifique `openstack`;
- Em **Application and OS Images** selecione `Ubuntu`;
- Em **Instance type** selecione `t2.large`;
- Em **Key Pair** selecione `openstack`;
- Em **Network Settings** selecione **Select existing security group** e selecione `openstack`;
- Em **Cofnigure storage** modifique o valor `8` por `100` - isso pode modificar a página, mas o campo continua o mesmo;
- Clique no botão laranja **Launch instance**.
